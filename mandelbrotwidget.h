/*  mandelbrotgl -  Mandelbrot Set Viewer
    Copyright (C) 2016  Alois Wohlschlager, Sara Fish

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MANDELBROTWIDGET_H
#define MANDELBROTWIDGET_H

#include <QMouseEvent>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <QStatusBar>

class MandelbrotWidget : public QOpenGLWidget, protected QOpenGLFunctions {
    Q_OBJECT

private:
    QOpenGLShaderProgram *program;
    float xmin;
    float xmax;
    float ymin;
    float ymax;
    float oldxmin;
    float oldxmax;
    float oldymin;
    float oldymax;
    int lastx;
    int lasty;
    bool doubleclick;

public:
    MandelbrotWidget(QWidget *parent = 0);
    ~MandelbrotWidget();
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;

public slots:
    void setColorScheme(int colorScheme);
};

#endif // MANDELBROTWIDGET_H
