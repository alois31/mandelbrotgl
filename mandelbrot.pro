#-------------------------------------------------
#
# Project created by QtCreator 2016-07-26T08:33:48
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mandelbrot
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        mandelbrotwidget.cpp

HEADERS  += mainwindow.h \
         mandelbrotwidget.h

FORMS    += mainwindow.ui

win32: {
    LIBS += -lopengl32
}

android: {
    LIBS += -lGLESv2
    DEFINES += SLOW_GRAPHICS
}
