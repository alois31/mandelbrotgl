/*  mandelbrotgl -  Mandelbrot Set Viewer
  Copyright (C) 2016  Alois Wohlschlager, Sara Fish

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mandelbrotwidget.h"

#define ZOOM_FACTOR 2.0

#ifdef SLOW_GRAPHICS
const int iterations = 2048;
#else
const int iterations = 8192;
#endif

const QString vertex =
    "attribute vec2 position;"
    "void main() {"
    "  gl_Position = vec4(position*2.0-1.0, 0.0, 1.0);"
    "}";

QString fragmentShaderHead =
    "uniform int iterations;\n"
    "\n"
    "uniform vec2 resolution;\n"
    "uniform float xmin;\n"
    "uniform float xmax;\n"
    "uniform float ymin;\n"
    "uniform float ymax;\n"
    "\n"
    "vec4 fragmentColor(float);\n"
    "\n"
    "void main()\n"
    "{\n"
    "  vec2 c = gl_FragCoord.xy / resolution;\n"
    "  float cre = (xmax - xmin) * c.x + xmin;\n"
    "  float cim = (ymax - ymin) * c.y + ymin;\n"
    "  int exceededAfter = -1;\n"
    "  float re = 0.0;\n"
    "  float im = 0.0;\n"
    "  float re_;\n"
    "  float norm;\n"
    "  for (int i=0; i<iterations; i++)\n"
    "    {\n"
    "      re_ = re;\n"
    "      re = re_ * re_ - im * im + cre;\n"
    "      im = 2.0 * re_ * im + cim;\n"
    "      norm = re * re + im * im;\n"
    "      if (norm > 4.0) \n"
    "        {\n"
    "          exceededAfter = i;\n"
    "          break;\n"
    "        }\n"
    "    }\n"
    "  vec4 color;\n"
    "  if (norm <= 4.0)\n"
    "    {\n"
    "      color = vec4(0.0, 0.0, 0.0, 1.0);\n"
    "    }\n"
    "  else\n"
    "    {\n"
    "      // The interpolation formula is not completely exact\n"
    "      // So we do 3 more iterations\n"
    "      re_ = re;\n"
    "      re = re_ * re_ - im * im + cre;\n"
    "      im = 2.0 * re_ * im + cim;\n"
    "      re_ = re;\n"
    "      re = re_ * re_ - im * im + cre;\n"
    "      im = 2.0 * re_ * im + cim;\n"
    "      re_ = re;\n"
    "      re = re_ * re_ - im * im + cre;\n"
    "      im = 2.0 * re_ * im + cim;\n"
    "      norm = re * re + im * im;\n"
    "      float interpolated = float(exceededAfter) + 3.0 - log(log(norm)\n"
    "                                                            / 2.0\n"
    "                                                            / log(2.0))\n"
    "                                                        / log(2.0);\n"
    "      color = fragmentColor(interpolated);\n"
    "    }\n"
    "  gl_FragColor = color;\n"
    "}\n"
    "\n"
    "float interpolate(float x, float ll, float valueLL, float l,\n"
    "                  float valueL, float r, float valueR, float rr,\n"
    "                  float valueRR)\n"
    "{\n"
    "  // Normalizing to [0, 1]\n"
    "  float slopeL = (valueLL - valueL) / (ll - l) * (r - l);\n"
    "  float slopeM = (valueL - valueR) / (l - r) * (r - l);\n"
    "  float slopeR = (valueR - valueRR) / (r - rr) * (r - l);\n"
    "  float xn = (x - l) / (r - l);\n"
    "  float linearPart = xn * slopeM + valueL;\n"
    "  float tangentL = (slopeL - slopeM) / 2.0;\n"
    "  float tangentR = (slopeR - slopeM) / 2.0;\n"
    "  float d = 0.0;\n"
    "  float c = tangentL;\n"
    "  float a = tangentL + tangentR;\n"
    "  float b = - 2.0 * tangentL - tangentR;\n"
    "  float cubicPart = xn * (xn * (a * xn + b) + c) + d;\n"
    "  if (r == l)\n"
    "    {\n"
    "      linearPart = valueL;\n"
    "      cubicPart = 0.0;\n"
    "    }\n"
    "  return linearPart + cubicPart;\n"
    "}\n"
    "\n"
    "vec4 interpolateColor(float x, float ll, vec4 colorLL, float l,\n"
    "                      vec4 colorL, float r, vec4 colorR, float rr,\n"
    "                      vec4 colorRR)\n"
    "{\n"
    "  return vec4(interpolate(x, ll, colorLL.x, l, colorL.x, r, colorR.x,\n"
    "                          rr, colorRR.x),\n"
    "              interpolate(x, ll, colorLL.y, l, colorL.y, r, colorR.y,\n"
    "                          rr, colorRR.y),\n"
    "              interpolate(x, ll, colorLL.z, l, colorL.z, r, colorR.z,\n"
    "                          rr, colorRR.z),"
    "              1.0);\n"
    "}\n"
    "\n";

const QString fragmentShaderColorImplBw =
    "vec4 fragmentColor(float iterations)\n"
    "{\n"
    "  return vec4(1.0, 1.0, 1.0, 1.0);\n"
    "}\n";

const QString fragmentShaderColorImplGrey =
    "vec4 fragmentColor(float iterations)\n"
    "{\n"
    "  float brightness = 1.0 - pow(0.99, iterations);\n"
    "  return vec4(brightness, brightness, brightness, 1.0);\n"
    "}\n";

const QString fragmentShaderColorImplRainbow =
    "vec4 interpolateColor(float, float, vec4, float, vec4, float, vec4,\n"
    "                      float, vec4);\n"
    "vec4 fragmentColor(float iterations)\n"
    "{\n"
    "  vec4 red = vec4(1.0, 0.0, 0.0, 1.0);\n"
    "  vec4 orange = vec4(1.0, 0.5, 0.0, 1.0);\n"
    "  vec4 yellow = vec4(1.0, 1.0, 0.0, 1.0);\n"
    "  vec4 green = vec4(0.0, 1.0, 0.0, 1.0);\n"
    "  vec4 blue = vec4(0.0, 0.0, 1.0, 1.0);\n"
    "  vec4 indigo = vec4(0.25, 0.0, 0.5, 1.0);\n"
    "  vec4 violet = vec4(1.0, 0.0, 1.0, 1.0);\n"
    "  float fractional = mod(iterations / 7.0, 1.0);\n"
    "  if (fractional < 0.142)\n"
    "    {\n"
    "      return interpolateColor(fractional, -0.143, violet, 0.0,\n"
    "                              red, 0.142, orange, 0.285, yellow);\n"
    "    }\n"
    "  else if (fractional < 0.285)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.0, red, 0.142,\n"
    "                              orange, 0.285, yellow, 0.428, green);\n"
    "    }\n"
    "  else if (fractional < 0.428)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.142, orange, 0.285,\n"
    "                              yellow, 0.428, green, 0.571, blue);\n"
    "    }\n"
    "  else if (fractional < 0.571)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.285, yellow, 0.428,\n"
    "                              green, 0.571, blue, 0.714, indigo);\n"
    "    }\n"
    "  else if (fractional < 0.714)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.428, green, 0.571,\n"
    "                              blue, 0.714, indigo, 0.857, violet);\n"
    "    }\n"
    "  else if (fractional < 0.857)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.571, blue, 0.714,\n"
    "                              indigo, 0.857, violet, 1.0, red);\n"
    "    }\n"
    "  else\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.714, indigo, 0.857,\n"
    "                              violet, 1.0, red, 1.142, orange);\n"
    "    }\n"
    "}\n";

const QString fragmentShaderColorImplOrange =
    "vec4 interpolateColor(float, float, vec4, float, vec4, float, vec4,\n"
    "                      float, vec4);\n"
    "vec4 fragmentColor(float iterations)\n"
    "{\n"
    "  vec4 c0 = vec4(0.0, 0.027, 0.391, 1.0);\n"
    "  vec4 c1 = vec4(0.125, 0.418, 0.793, 1.0);\n"
    "  vec4 c2 = vec4(0.926, 1.0, 1.0, 1.0);\n"
    "  vec4 c3 = vec4(1.0, 0.664, 0.0, 1.0);\n"
    "  vec4 c4 = vec4(0.0, 0.008, 0.0, 1.0);\n"
    "  float fractional = mod(iterations / 64.0, 1.0);\n"
    "  if (fractional < 0.16)\n"
    "    {\n"
    "      return interpolateColor(fractional, -0.142, c4, 0.0, c0, 0.16,\n"
    "                              c1, 0.42, c2);\n"
    "    }\n"
    "  else if (fractional < 0.42)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.0, c0, 0.16, c1, 0.42, c2,\n"
    "                              0.643, c3);\n"
    "    }\n"
    "  else if (fractional < 0.643)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.16, c1, 0.42, c2, 0.643,\n"
    "                              c3, 0.858, c4);\n"
    "    }\n"
    "  else if (fractional < 0.858)\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.42, c2, 0.643, c3, 0.858,\n"
    "                              c4, 1.0, c0);\n"
    "    }\n"
    "  else\n"
    "    {\n"
    "      return interpolateColor(fractional, 0.643, c3, 0.858, c4, 1.0,\n"
    "                              c0, 1.16, c1);\n"
    "    }\n"
    "}\n";

MandelbrotWidget::~MandelbrotWidget()
{
  delete program;
}

MandelbrotWidget::MandelbrotWidget(QWidget *parent)
  : QOpenGLWidget(parent)
{
}

void MandelbrotWidget::initializeGL()
{
  xmin = -2.0;
  xmax = 0.7;
  ymin = -1.2;
  ymax = 1.2;
  doubleclick = false;
  initializeOpenGLFunctions();
  program = new QOpenGLShaderProgram;
  program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertex);
  program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                   fragmentShaderHead);
  program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                   fragmentShaderColorImplBw);
  program->link();
}

void MandelbrotWidget::paintGL()
{
  program->bind();
  program->setUniformValue("iterations", iterations);
  program->setUniformValue("resolution", QVector2D(width(), height()));
  program->setUniformValue("xmin", xmin);
  program->setUniformValue("xmax", xmax);
  program->setUniformValue("ymin", ymin);
  program->setUniformValue("ymax", ymax);
  GLfloat rect [] = {
    0.0, 0.0,
    1.0, 0.0,
    1.0, 1.0,
    0.0, 1.0
  };
  GLint positionAttribute = program->attributeLocation("position");
  glEnableVertexAttribArray(positionAttribute);
  glVertexAttribPointer(positionAttribute, 2, GL_FLOAT, false, 0, rect);
  glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
  glDisableVertexAttribArray(positionAttribute);
  program->release();
}

void MandelbrotWidget::resizeGL(int width, int height)
{
  glViewport(0.0, 0.0, width, height);
}

void MandelbrotWidget::mousePressEvent(QMouseEvent *event)
{
  lastx = event->x();
  lasty = height() - event->y();
}

void MandelbrotWidget::mouseDoubleClickEvent(QMouseEvent *event) {
  doubleclick = true;
  int x = event->x();
  int y = height() - event->y();
  float x_ = (float) x / width() * (oldxmax - oldxmin) + oldxmin;
  float y_ = (float) y / height() * (oldymax - oldymin) + oldymin;
  xmin = x_ - (oldxmax - oldxmin) / ZOOM_FACTOR / 2.0;
  xmax = x_ + (oldxmax - oldxmin) / ZOOM_FACTOR / 2.0;
  ymin = y_ - (oldymax - oldymin) / ZOOM_FACTOR / 2.0;
  ymax = y_ + (oldymax - oldymin) / ZOOM_FACTOR / 2.0;
  repaint();
}

void MandelbrotWidget::mouseReleaseEvent(QMouseEvent *event) {
  if (doubleclick)
    {
     doubleclick = false;
    }
  else
    {
      oldxmin = xmin;
      oldxmax = xmax;
      oldymin = ymin;
      oldymax = ymax;
      int x = event->x();
      int y = height() - event->y();
      if (x == lastx || y == lasty)
        {  //if just click, resets to normal
          xmin = -2.0;
          xmax = 0.7;
          ymin = -1.2;
          ymax = 1.2;
        }
      else
        {
          float x_ = (float) x / width() * (xmax - xmin) + xmin;
          float lastx_ = (float) lastx / width() * (xmax - xmin) + xmin;
          float y_ = (float) y / height() * (ymax - ymin) + ymin;
          float lasty_ = (float) lasty / height() * (ymax - ymin) + ymin;
          if (x < lastx)
            {
              xmin = x_;
              xmax = lastx_;
            }
          else
            {
              xmin = lastx_;
              xmax = x_;
            }
            if (y < lasty)
              {
              ymin = y_;
              ymax = lasty_;
              }
            else
              {
                ymin = lasty_;
                ymax = y_;
              }
        }
      repaint();
    }
}

void MandelbrotWidget::setColorScheme(int colorScheme)
{
  if (colorScheme == 0)
    {
      program->removeAllShaders();
      program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertex);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderHead);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderColorImplBw);
    }
  else if (colorScheme == 1)
    {
      program->removeAllShaders();
      program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertex);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderHead);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderColorImplGrey);
    }
  else if (colorScheme == 2)
    {
      program->removeAllShaders();
      program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertex);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderHead);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderColorImplRainbow);
    }
  else if (colorScheme == 3)
    {
      program->removeAllShaders();
      program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertex);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderHead);
      program->addShaderFromSourceCode(QOpenGLShader::Fragment,
                                       fragmentShaderColorImplOrange);
    }
  repaint();
}
